// Getting the require dependencies
// This is used for communicating with the Mongo DB
const mongoose = require("mongoose");

// This brings in the express server side framework
const express = require("express");

// Used for parsing the request body in HTTP communication
const bodyParser = require("body-parser");

// Importing some global utils
const { mongoURI } = require("./src/utils/mongo");
const { appPort } = require("./src/utils/appVariables");

const cors = require("cors");

const app = express();
console.log(mongoURI);

const db = mongoose.connect(mongoURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const port = process.env.PORT || appPort;
const Metric = require("./src/models/metricModel");

// Handles the actual requests
const metricROuter = require("./src/routes/metricRouter")(Metric);

app.use(cors());

// Handling the HTTP body
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Tells the applciation which path to use for API recognition
app.use("/api", metricROuter);

app.get("/", (req, res) => {
  res.send("Hello from my monitoring API!");
});

app.listen(port, () => {
  console.log(`Running on port ${port} - ${Date.now()}`);
});