const appVariables = {
  appPort: 5000,
  baseUrl: `localhost:${this.appPort}/api/overtimes/`,
};

module.exports = appVariables;
