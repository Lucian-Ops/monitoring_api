function metricController(Metric) {
  // Save an metric object on the DB
  function postMetric(req, res) {
    if (req.body._id) {
      delete req.body._id;
    }

    const metric = new Metric(req.body);

    metric.save((err) => {
      if (err) {
        return res.send(err);
      }
      res.status(201);
      return res.send(metric);
    });
  }

  function getMetric(req, res) {
    let query = {};

    Metric.find(query, (err, metrics) => {
      if (err) {
        return res.send(err);
      }
      return res.json(metrics);
    });
  }

  function getReport5(req, res) {
    let query = {};

    Metric.find(query, (err, metrics) => {
      if (err) {
        return res.send(err);
      }

      let averageT = 0;
      let count = 0;
      let dt = new Date();
      let newDt = new Date(dt.getTime() - 5 * 60000);

      metrics.forEach((element) => {
        let timestamp = element._id.toString().substring(0, 8);
        let date = new Date(parseInt(timestamp, 16) * 1000);

        if (date > newDt) {
          averageT += element.measuredTemperatureC;
          count++;
        }
      });

      if (!count) return res.json("No data avaialable");
      return res.json(averageT / count).toString();
    });
  }

  return { postMetric, getMetric, getReport5 };
}

module.exports = metricController;
