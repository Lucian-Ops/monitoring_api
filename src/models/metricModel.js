const mongoose = require("mongoose");

// The expected Mongo DB objects
const { Schema } = mongoose;

const metricModel = new Schema({
  measuredTime: { type: Date, required: true },
  measuredTemperatureC: { type: Number, required: true },
  measuredCO2: { type: Number, required: true },
  measuredHumidity: { type: Number, required: true },
  measuredPressure: { type: Number, required: false },
});

module.exports = mongoose.model("Metric", metricModel);
