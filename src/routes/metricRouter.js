const express = require("express");
const metricController = require("../controllers/metricController");

function routes(metric) {
  const metricRouter = express.Router();

  // The actions to be taken on each called method
  const controller = metricController(metric);

  // What to do on each route
  metricRouter
    .route("/metrics")
    .post(controller.postMetric)
    .get(controller.getMetric);

  metricRouter.route("/getReport5").get(controller.getReport5);

  return metricRouter;
}

module.exports = routes;
